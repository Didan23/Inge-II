package sdgp.entity;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import sdgp.entity.Proyectos;
import sdgp.entity.Tareas;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-09-23T21:10:27")
@StaticMetamodel(Estados.class)
public class Estados_ { 

    public static volatile SingularAttribute<Estados, String> descripcionEstado;
    public static volatile SingularAttribute<Estados, BigDecimal> idEstado;
    public static volatile CollectionAttribute<Estados, Tareas> tareasCollection;
    public static volatile SingularAttribute<Estados, Proyectos> proyectos;

}