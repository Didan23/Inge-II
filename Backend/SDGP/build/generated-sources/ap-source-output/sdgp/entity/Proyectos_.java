package sdgp.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import sdgp.entity.Estados;
import sdgp.entity.Tareas;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-09-23T21:10:27")
@StaticMetamodel(Proyectos.class)
public class Proyectos_ { 

    public static volatile SingularAttribute<Proyectos, BigDecimal> idProyecto;
    public static volatile CollectionAttribute<Proyectos, Tareas> tareasCollection;
    public static volatile SingularAttribute<Proyectos, String> nombreProyecto;
    public static volatile SingularAttribute<Proyectos, Date> fechaCreacion;
    public static volatile SingularAttribute<Proyectos, BigInteger> estadoProyecto;
    public static volatile SingularAttribute<Proyectos, Date> fechaFinalizacion;
    public static volatile SingularAttribute<Proyectos, Estados> estados;

}