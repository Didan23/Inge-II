package sdgp.entity;

import java.math.BigDecimal;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import sdgp.entity.Usuarios;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-09-23T21:10:27")
@StaticMetamodel(Roles.class)
public class Roles_ { 

    public static volatile SingularAttribute<Roles, BigDecimal> idRol;
    public static volatile SingularAttribute<Roles, String> nombreRol;
    public static volatile CollectionAttribute<Roles, Usuarios> usuariosCollection;

}