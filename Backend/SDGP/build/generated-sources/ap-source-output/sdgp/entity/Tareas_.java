package sdgp.entity;

import java.math.BigDecimal;
import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import sdgp.entity.Estados;
import sdgp.entity.Proyectos;
import sdgp.entity.Usuarios;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-09-23T21:10:27")
@StaticMetamodel(Tareas.class)
public class Tareas_ { 

    public static volatile SingularAttribute<Tareas, Proyectos> idProyecto;
    public static volatile SingularAttribute<Tareas, Date> fechaModificacion;
    public static volatile SingularAttribute<Tareas, String> tareaDescripcion;
    public static volatile SingularAttribute<Tareas, BigDecimal> idTarea;
    public static volatile SingularAttribute<Tareas, Usuarios> idUsuario;
    public static volatile SingularAttribute<Tareas, String> nombreTarea;
    public static volatile SingularAttribute<Tareas, Estados> estadoTarea;
    public static volatile SingularAttribute<Tareas, Date> fechaCreacion;

}