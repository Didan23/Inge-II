package sdgp.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import sdgp.entity.Roles;
import sdgp.entity.Tareas;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2018-09-23T21:10:27")
@StaticMetamodel(Usuarios.class)
public class Usuarios_ { 

    public static volatile SingularAttribute<Usuarios, String> password;
    public static volatile CollectionAttribute<Usuarios, Tareas> tareasCollection;
    public static volatile SingularAttribute<Usuarios, BigDecimal> idUsuario;
    public static volatile SingularAttribute<Usuarios, String> correo;
    public static volatile SingularAttribute<Usuarios, BigInteger> estadoUsuario;
    public static volatile SingularAttribute<Usuarios, String> nombreUsuario;
    public static volatile SingularAttribute<Usuarios, Roles> idRolUsuario;

}